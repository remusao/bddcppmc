#include <iostream>
#include "bddLib/bdd.hh"
#include <list>
#include <stdio.h>

int N = 5;
Bdd::Bdd** cell; // pre-state board.

namespace
{
    // We define our structure (x,y) that represents a cell of the board.
    typedef struct
    {
        int x;
        int y;
        bool** passed;
    }       Coord;

    bool**
    mcopy(bool** t)
    {
        bool** ret = new bool*[N];;
        for (int i = 0; i < N; ++i)
        {
            ret[i] = new bool[N];
            memcpy(ret[i], t[i], N * sizeof (bool));
        }

        return ret;
    }

    bool passedEverywhere(bool** t)
    {
        for (int x = 0; x < N; ++x)
            for (int y = 0; y < N; ++y)
                if (!t[x][y])
                    return false;
        return true;
    }

    // We only want cells inside the board.
    bool
    isNotOnBoard(Coord& value)
    {
        return value.x < 0 || value.x >= N
            || value.y < 0 || value.y >= N
            || value.passed[value.x][value.y];
    }

    // We get the next moves of our knight for a Coord (x, y)
    std::list<Coord>
    nextMoves(Coord& c)
    {
        std::list<Coord> moves =
           {{c.x + 2, c.y + 1, mcopy(c.passed)},
            {c.x - 2, c.y + 1, mcopy(c.passed)},
            {c.x + 2, c.y - 1, mcopy(c.passed)},
            {c.x - 2, c.y - 1, mcopy(c.passed)},
            {c.x + 1, c.y + 2, mcopy(c.passed)},
            {c.x - 1, c.y + 2, mcopy(c.passed)},
            {c.x + 1, c.y - 2, mcopy(c.passed)},
            {c.x - 1, c.y - 2, mcopy(c.passed)}};
        moves.remove_if(isNotOnBoard);

        return moves;
    }

    Bdd::Bdd
    move(Coord& d)
    {
        Bdd::Bdd v1 = cell[d.x][d.y]; // destination

        return v1;
    }

    Bdd::Bdd
    transition(Coord& s)
    {
        std::list<Coord> moves = nextMoves(s);
        Bdd::Bdd ret = Bdd::getFalseBdd();

        for (Coord d : moves)
            ret = ret || move(d);

        return ret;
    }

    Bdd::Bdd
    tour(Coord& s, Bdd::Bdd path)
    {
        if (passedEverywhere(s.passed))
            return Bdd::getTrueBdd();

        s.passed[s.x][s.y] = true;

        std::list<Coord> moves = nextMoves(s);

        path = path && transition(s);
        for (Coord d : moves)
            path = path && tour(d, path);

        return path;
    }
}

int main(int argc, char** argv)
{
    std::cout << "Knight's Tour" << std::endl;
    bool** ptrs;

    if (argc < 2)
    {
        std::cerr << "Usage: " << argv[0]
            << " <size of the board>" << std::endl;
        return 1;
    }

    N = atoi(argv[1]);

    // Init and fill of the board.
    Bdd::initBdd(2 * N * N);

    cell = new Bdd::Bdd*[N];
    for (int line = 0; line < N; ++line)
        cell[line] = new Bdd::Bdd[N];

    ptrs = new bool*[N];
    for (int line = 0; line < N; ++line)
        ptrs[line] = new bool[N];

    int i = -1;
    for (int x = 0; x < N; ++x)
        for (int y = 0; y < N; ++y)
        {
            cell[x][y] = Bdd::bdd_ithvar(++i);
            ptrs[x][y] = false;
        }

    Coord c = {0, 0, ptrs};
    Bdd::Bdd ret = tour(c, Bdd::getTrueBdd());

    // Print the results
    ret.print();
    std::cout << "There are " << ret.sat() << " solution(s)" << std::endl;
    std::cout << "one is:" << std::endl;
    auto any_sat = ret.any_sat();
    for (std::pair<int, bool> pair : any_sat)
        std::cout << "Var num : " << pair.first << " val : " << pair.second << std::endl;

    Bdd::releaseBdd();

    return 0;
}
