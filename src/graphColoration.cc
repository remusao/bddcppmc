#include <iostream>
#include <string>
#include "bddLib/bdd.hh"

#define SIZE (49 * 4)

namespace
{
    Bdd::Bdd graph = Bdd::getTrueBdd();
    Bdd::Bdd states[SIZE];

    int AL = 0 * 4;
    int AZ = 1 * 4;
    int AR = 2 * 4;
    int CA = 3 * 4;
    int CO = 4 * 4;
    int CT = 5 * 4;
    int DE = 6 * 4;
    int FL = 7 * 4;
    int GA = 8 * 4;
    int ID = 9 * 4;
    int IL = 10 * 4;
    int IN = 11 * 4;
    int IA = 12 * 4;
    int KS = 13 * 4;
    int KY = 14 * 4;
    int LA = 15 * 4;
    int ME = 16 * 4;
    int MD = 17 * 4;
    int MA = 18 * 4;
    int MI = 19 * 4;
    int MN = 20 * 4;
    int MS = 21 * 4;
    int MO = 22 * 4;
    int MT = 23 * 4;
    int NE = 24 * 4;
    int NV = 25 * 4;
    int NH = 26 * 4;
    int NJ = 27 * 4;
    int NM = 28 * 4;
    int NY = 29 * 4;
    int NC = 30 * 4;
    int ND = 31 * 4;
    int OH = 32 * 4;
    int OK = 33 * 4;
    int OR = 34 * 4;
    int PA = 35 * 4;
    int RI = 36 * 4;
    int SC = 37 * 4;
    int SD = 38 * 4;
    int TN = 39 * 4;
    int TX = 40 * 4;
    int UT = 41 * 4;
    int VT = 42 * 4;
    int VA = 43 * 4;
    int WA = 44 * 4;
    int WV = 45 * 4;
    int WI = 46 * 4;
    int WY = 47 * 4;

    Bdd::Bdd edge(unsigned a, unsigned b)
    {
        Bdd::Bdd
            state1Color1, state1Color2, state1Color3, state1Color4,
            state2Color1, state2Color2, state2Color3, state2Color4;

        // One state, one color
        state1Color1 = states[a + 0];
        state1Color2 = states[a + 1];
        state1Color3 = states[a + 2];
        state1Color4 = states[a + 3];

        state2Color1 = states[b + 0];
        state2Color2 = states[b + 1];
        state2Color3 = states[b + 2];
        state2Color4 = states[b + 3];

        std::cout << "edge(" << a << ", " << b << ")" << std::endl;

        return
           state1Color1.imply(!state2Color1)
        && state1Color2.imply(!state2Color2)
        && state1Color3.imply(!state2Color3)
        && state1Color4.imply(!state2Color4)
        && (state1Color1 || state1Color2 || state1Color3 || state1Color4)
        && (state2Color1 || state2Color2 || state2Color3 || state2Color4);
    }


    Bdd::Bdd createGraph()
    {
        return edge(AL, FL) && edge(AL, GA) && edge(AL, MS) && edge(AL, TN) &&
        edge(AR, LA) && edge(AR, MO) && edge(AR, MS) && edge(AR, OK) &&
        edge(AR, TN) && edge(AR, TX) && edge(AZ, CA) && edge(AZ, NM) &&
        edge(AZ, NV) && edge(AZ, UT) && edge(CA, NV) && edge(CA, OR) &&
        edge(CO, KS) && edge(CO, NE) && edge(CO, NM) && edge(CO, OK) &&
        edge(CO, UT) && edge(CO, WY) && edge(CT, MA) && edge(CT, NY) &&
        edge(CT, RI) && edge(DE, MD) && edge(DE, NJ) && edge(DE, PA) &&
        edge(FL, GA) && edge(GA, NC) && edge(GA, SC) && edge(GA, TN) &&
        edge(IA, IL) && edge(IA, MN) && edge(IA, MO) && edge(IA, NE) &&
        edge(IA, SD) && edge(IA, WI) && edge(ID, MT) && edge(ID, NV) &&
        edge(ID, OR) && edge(ID, UT) && edge(ID, WA) && edge(ID, WY) &&
        edge(IL, IN) && edge(IL, KY) && edge(IL, MO) && edge(IL, WI) &&
        edge(IN, KY) && edge(IN, MI) && edge(IN, OH) && edge(KS, MO) &&
        edge(KS, NE) && edge(KS, OK) && edge(KY, MO) && edge(KY, OH) &&
        edge(KY, TN) && edge(KY, VA) && edge(KY, WV) && edge(LA, MS) &&
        edge(LA, TX) && edge(MA, NH) && edge(MA, NY) && edge(MA, RI) &&
        edge(MA, VT) && edge(MD, PA) && edge(MD, VA) && edge(MD, WV) &&
        edge(ME, NH) && edge(MI, OH) && edge(MI, WI) && edge(MN, ND) &&
        edge(MN, SD) && edge(MN, WI) && edge(MO, NE) && edge(MO, OK) &&
        edge(MO, TN) && edge(MS, TN) && edge(MT, ND) && edge(MT, SD) &&
        edge(MT, WY) && edge(NC, SC) && edge(NC, TN) && edge(NC, VA) &&
        edge(ND, SD) && edge(NE, SD) && edge(NE, WY) && edge(NH, VT) &&
        edge(NJ, NY) && edge(NJ, PA) && edge(NM, OK) && edge(NM, TX) &&
        edge(NV, OR) && edge(NV, UT) && edge(NY, PA) && edge(NY, VT) &&
        edge(OH, PA) && edge(OH, WV) && edge(OK, TX) && edge(OR, WA) &&
        edge(PA, WV) && edge(SD, WY) && edge(TN, VA) && edge(UT, WY) &&
        edge(VA, WV);
    }
}

int
main(int, char *[])
{
    std::cout << "Graph coloration" << std::endl;

    // 48 states * 4 colors
    Bdd::initBdd(SIZE);

    std::cout << "Init variables" << std::endl;
    for (unsigned i = 0; i < SIZE; ++i)
        states[i] = Bdd::bdd_ithvar(i);

    std::cout << "Create the graph" << std::endl;
    Bdd::Bdd usa = createGraph();

    std::cout << "There are " << usa.sat() << " solutions\n";
    std::cout << "one is:" << std::endl;
    auto any_sat = usa.any_sat();
    for (std::pair<int, bool> pair : any_sat)
        std::cout << "Var num : " << pair.first << " val : " << pair.second << std::endl;

    return 0;
}
