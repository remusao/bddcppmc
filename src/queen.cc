#include <iostream>
#include <sstream>
#include "bddLib/bdd.hh"

int nb_queens = 8;
Bdd::Bdd a, b, c, d;
Bdd::Bdd **cells;
Bdd::Bdd queen;

namespace
{
  void build_queen_at(int x, int y)
  {
    for (int l = 0 ; l < nb_queens; ++l)
      if (l != y)
        a = a && (cells[x][y].implyNot(cells[x][l]));

    for (int k = 0 ; k < nb_queens; ++k)
      if (k != x)
        b = b && (cells[x][y].implyNot(cells[k][y]));

    /* No one in the same up-right diagonal */
    for (int k = 0 ; k < nb_queens; ++k)
    {
      int ll = k - x + y;
      if (ll >= 0 && ll < nb_queens)
        if (k != x)
          c = c && (cells[x][y].implyNot(cells[k][ll]));
    }

    /* No one in the same down-right diagonal */
    for (int k = 0 ; k < nb_queens; ++k)
    {
      int ll = x + y - k;
      if (ll >= 0 && ll < nb_queens)
        if (k != x)
          d = d && (cells[x][y].implyNot(cells[k][ll]));
    }
    queen = queen && a && b && c && d;
  }
}

int main(int argc, char** argv)
{
  std::cout << "Queens" << std::endl;


  if (argc < 2)
  {
    std::cerr << "Usage: " << argv[0]
              << " <number of queens>" << std::endl;
    exit(1);
  }

  nb_queens = atoi(argv[1]);

  if (nb_queens <= 8)
    Bdd::initBdd(nb_queens * nb_queens, 2097152);
  else
    Bdd::initBdd(nb_queens * nb_queens);

  cells = new Bdd::Bdd*[nb_queens];
  for (int line = 0; line < nb_queens; ++line)
    cells[line] = new Bdd::Bdd[nb_queens];


  a = Bdd::getTrueBdd();
  b = Bdd::getTrueBdd();
  c = Bdd::getTrueBdd();
  d = Bdd::getTrueBdd();

  queen = Bdd::getTrueBdd();

  int i = 0;
  for (int x = 0; x < nb_queens; ++x)
    for (int y = 0; y < nb_queens; ++y)
      cells[x][y] = Bdd::bdd_ithvar(i++);

  /* Place a queen in each row */
  for (int r = 0 ; r < nb_queens; ++r)
  {
    Bdd::Bdd e = Bdd::getFalseBdd();
    for (int l = 0; l < nb_queens; ++l)
    {
      std::cout << "Placing queen at " << r << "," << l
        << std::endl;
      e = e || cells[r][l];
    }
    queen = queen && e;
  }

  /* Build requirements for each variable(field) */
  for (int r = 0 ; r < nb_queens; ++r)
    for (int l = 0; l < nb_queens; ++l)
    {
      std::cout << "Adding position " << r << ","
                << l << std::endl;
      build_queen_at(r, l);
    }

  /* Print the results */
  std::cout << "There are " << queen.sat() << " solutions\n";
  std::cout << "one is:" << std::endl;
  auto any_sat = queen.any_sat();
  for (std::pair<int, bool> pair : any_sat)
    std::cout << "Var num : " << pair.first << " val : " << pair.second << std::endl;

  Bdd::releaseBdd();
}
