#pragma once

#ifndef BDD_HH
# define BDD_HH

# include "bddNode.hh"
# include "bddUtils.hh"
# include "memoryManager.hh"

namespace Bdd
{

    // Class for a BDD structure
    class Bdd
    {
        public:
            Bdd() noexcept;
            Bdd(struct BddNode* root) noexcept;
            ~Bdd() noexcept;

            // Return the unique nodes on true and false
            // static struct BddNode* getTrueBdd() noexcept;
            // static struct BddNode* getFalseBdd() noexcept;

            struct BddNode* getRoot() const noexcept;

            Bdd operator!() const noexcept;
            Bdd operator||(const Bdd& bdd) const noexcept;
            Bdd operator&&(const Bdd& bdd) const noexcept;
            Bdd operator^(const Bdd& bdd) const noexcept;

            // Exist quantification
            Bdd exist(std::size_t var) const noexcept;
            // Imply
            Bdd imply(const Bdd& bdd) const noexcept;
            Bdd implyNot(const Bdd& bdd) const noexcept;
            // Equivalence
            Bdd equivalence(const Bdd& bdd) const noexcept;

            void print() const noexcept;
            std::vector<std::pair<unsigned, bool>> any_sat() const noexcept;
            std::vector<std::vector<std::pair<unsigned, bool>>> all_sat() const noexcept;
            unsigned sat() const noexcept;

        private:

            struct BddNode* root_; // Root of this BDD
    };

    void initBdd(std::size_t nbVars, std::size_t poolSize = 134217728) noexcept;
    void releaseBdd() noexcept;

    inline Bdd bdd_ithvar(std::size_t variable) noexcept
    {
        return Bdd(bdd_ithvarnode(variable));
    }

    inline Bdd bdd_not_ithvar(std::size_t variable) noexcept
    {
        return Bdd(bdd_not_ithvarnode(variable));
    }
}

#endif /* !BDD_HH */

