#ifndef MEMORY_MANAGER_HH_
# define MEMORY_MANAGER_HH_

#include <cstdlib>
#include <cstring>
#include <iostream>
#include <cassert>


namespace Bdd
{
    // Forward declaration
    template<class T>
    struct MemoryManager
    {
        std::size_t offset; // offset to look the the next free emplacement
        std::size_t nbNodes; // number of nodes stored
        std::size_t size; // size of the memory pool
        T* memory; // pointer on memory pool
    };


    // ini the structure of the memory manager
    template<class T>
    inline MemoryManager<T> initMM(std::size_t size) noexcept
    {
        std::cout << "Allocating pool of size " << size << " (" << sizeof (T) * size << ")" << std::endl;
        MemoryManager<T> mm{
            (std::size_t)-1,
            0,
            size,
            (T*)malloc(size * sizeof (T))
        };
        mm.memory = (T*)memset(mm.memory, 0, size * sizeof (T));
        return mm;
    }

    // Free the memory manager
    template<class T>
    inline void release(MemoryManager<T>& mm) noexcept
    {
        if (mm.memory != nullptr)
        {
            free(mm.memory);
            mm.memory = nullptr;
        }
    }

    // allocate a new node in the memory pool
    template<class T>
    struct BddNode* newNode(MemoryManager<T>& mm) noexcept
    {
      // If full, increase the size of the pool
      if (mm.nbNodes >= mm.size)
      {
        std::cout << "Out of memory" << std::endl;
        assert(0);
      }

      ++mm.nbNodes;
      // return the absolute pointer
      return (T*)(mm.memory + (++mm.offset));
    }


    // Get the pointer associated with this offset in the memory pool
    template<class T>
    inline T* getBddNode(MemoryManager<T>& mm, unsigned offset) noexcept
    {
        return (T*)(mm.memory + offset);
    }
    // Get the offset of the given pointer
    template<class T>
    inline std::size_t getOffset(MemoryManager<T>& mm, const T* node) noexcept
    {
        return (std::size_t)(node - mm.memory);
    }

}

#endif
