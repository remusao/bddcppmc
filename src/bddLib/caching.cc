#include "caching.hh"
#include "bddNode.hh"

namespace Bdd
{
//    std::unordered_map<std::size_t, struct BddNode*> Caching::__node2id;
    UniqueHashMap Caching::__node2id(200000);
    std::vector<std::size_t> Caching::__variables;
    struct BddNode* Caching::trueNode{nullptr};
    struct BddNode* Caching::falseNode{nullptr};
    MemoryManager<BddNode> Caching::g_mm;
}
