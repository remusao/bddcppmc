#pragma once

#ifndef BDDNODE_HH
# define BDDNODE_HH

# include <cstddef>
# include "caching.hh"

namespace Bdd
{
    struct BddNode
    {
        // Attributes
        unsigned var : 10;
        std::size_t false_ptr : 27;
        std::size_t true_ptr : 27;
    } __attribute__ ((packed));


    inline struct BddNode*
    createNode(const BddNode& node) noexcept
    {
        struct BddNode* n = newNode(Caching::g_mm);
        *n = node;

        return n;
    }


    inline struct BddNode*
    createNode(unsigned var, std::size_t f, std::size_t t) noexcept
    {
        struct BddNode* n = newNode(Caching::g_mm);
        *n = BddNode{var, f, t};

        return n;
    }


    inline struct BddNode*
    createNode(unsigned var, struct BddNode* f, struct BddNode* t) noexcept
    {
        return createNode(var, getOffset<BddNode>(Caching::g_mm, f), getOffset(Caching::g_mm, t));
    }


    inline struct BddNode*
    getFalseNode(const struct BddNode* node) noexcept
    {
        return getBddNode<BddNode>(Caching::g_mm, node->false_ptr);
    }


    inline struct BddNode*
    getFalseNode(std::size_t node) noexcept
    {
        return getBddNode<BddNode>(Caching::g_mm, getBddNode(Caching::g_mm, node)->false_ptr);
    }


    inline struct BddNode*
    getTrueNode(const struct BddNode* node) noexcept
    {
        return getBddNode<BddNode>(Caching::g_mm, node->true_ptr);
    }


    inline struct BddNode*
    getTrueNode(std::size_t node) noexcept
    {
        return getBddNode<BddNode>(Caching::g_mm, getBddNode(Caching::g_mm, node)->true_ptr);
    }


    inline unsigned
    getVarNode(const struct BddNode* node) noexcept
    {
        return node->var;
    }


    inline unsigned
    getVarNode(std::size_t node) noexcept
    {
        return getBddNode<BddNode>(Caching::g_mm, node)->var;
    }


    inline std::size_t
    magicCast(const struct BddNode* n) noexcept
    {
        return (n != 0 ? *(std::size_t*)(n) : 0);
    }


    inline void
    assign(unsigned i, struct BddNode* n) noexcept
    {
        if (n != 0)
            *(unsigned*)n = i;
    }
}



#endif /* !BDDNODE_HH */
