#pragma once

#ifndef BDDUTILS_HH
# define BDDUTILS_HH

# include <functional>
# include <unordered_map>
# include "bddNode.hh"

namespace Bdd
{
    class Bdd;

    /// Check if node is either True or False
    bool isLeaf(const BddNode* node) noexcept;
    bool isFalseBdd(const BddNode* node) noexcept;
    bool isTrueBdd(const BddNode* node) noexcept;

    /// Create a new node from (id, false_ptr, true_ptr) but ensure the unicity
    /// of the node by checking if it already exists in a hash table :
    /// 1) If it already exists, we juste return the existing node
    /// 2) If it doesn't exist, we create a new node, add it to the hash table
    /// and return it.
    BddNode* makeNode(unsigned id, BddNode* false_ptr, BddNode* true_ptr) noexcept;

    /// @brief bdd1 `op` bdd2
    /// The structure cache is used to store operations already performed.
    Bdd apply(
        const std::function<BddNode* (const BddNode*, const BddNode*)>& op,
        const Bdd& bdd1,
        const Bdd& bdd2) noexcept;

    void BreadthFirstSearch(
        const std::function<void (const BddNode*)>& op,
        const Bdd& bdd) noexcept;

    void DepthFirstSearch(
        const std::function<void (const BddNode*)>& op,
        const Bdd& bdd) noexcept;

    void bfs_ModifyBdd(
        const std::function<BddNode* (BddNode*)>& op,
        Bdd& bdd) noexcept;

    void dfs_ModifyBdd(
        const std::function<BddNode* (BddNode*)>& op,
        Bdd& bdd) noexcept;

    Bdd Restrict(
        const Bdd&      n,
        std::size_t     var,
        BddNode*  value) noexcept;

    void setVarNum(std::size_t nbVar) noexcept;

    inline BddNode*
    bdd_ithvarnode(std::size_t variable) noexcept
    {
        return makeNode(Caching::__variables[variable], getFalseBdd(), getTrueBdd());
    }

    inline BddNode*
    bdd_not_ithvarnode(std::size_t variable) noexcept
    {
        return makeNode(Caching::__variables[variable], getTrueBdd(), getFalseBdd());
    }
}

#endif /* !BDDUTILS_HH */
