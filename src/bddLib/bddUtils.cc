#include <queue>
#include "bddUtils.hh"
#include "bdd.hh"
#include "memoryManager.hh"
#include "caching.hh"

#include <iostream>

namespace Bdd
{
    bool
    isLeaf(const BddNode* node) noexcept
    {
        return (getVarNode(node) >= Caching::__variables.size());
    }

    bool
    isTrueBdd(const BddNode* node) noexcept
    {
        return getVarNode(node) == getVarNode(Caching::trueNode);
    }

    bool
    isFalseBdd(const BddNode* node) noexcept
    {
        return getVarNode(node) == getVarNode(Caching::falseNode);
    }


    BddNode*
    makeNode(unsigned id, BddNode* false_ptr, BddNode* true_ptr) noexcept
    {
        BddNode* res = nullptr;

        if (true_ptr != false_ptr)
        {
            // The node is its hash value. So with the union
            // we get the size_t representation of the node
            // (its size if 8 bytes so it's ok)
            union
            {
                std::size_t hash;
                struct BddNode node;
            };
            node = {id, getOffset(Caching::g_mm, false_ptr), getOffset(Caching::g_mm, true_ptr)};


            // Check if the newly created node isn't in the cache already
            auto it = Caching::__node2id.find(hash);

            if (it == Caching::__node2id.end())
            {
                // Create a new node and add it to the hash table
                BddNode* newNode = createNode(node);
                Caching::__node2id[hash] = newNode;
                res = newNode;
            }
            else
                res = it->second;
        }
        else
            res = false_ptr;

        return res;
    }

    // Anonymous namespace to hide implementation
    namespace
    {
        BddNode*
        app(
            std::unordered_map<std::size_t, BddNode*>& cache,
            const std::function<BddNode* (const BddNode*, const BddNode*)>& op,
            const BddNode* n1,
            const BddNode* n2) noexcept
        {
            BddNode* res = nullptr;

            if (isLeaf(n1) && isLeaf(n2))
                res = op(n1, n2);
            else
            {
                std::size_t hash =
                     ((std::size_t)(getOffset(Caching::g_mm, n1)) << 32)
                    | (std::size_t)(getOffset(Caching::g_mm, n2));
                auto it = cache.find(hash);

                if (it != cache.end())
                    res = it->second;
                else if (getVarNode(n1) == getVarNode(n2))
                {
                    res = makeNode(getVarNode(n1),
                            app(cache, op, getFalseNode(n1), getFalseNode(n2)),
                            app(cache, op, getTrueNode(n1), getTrueNode(n2)));
                    cache[hash] = res;
                }
                else if (getVarNode(n1) < getVarNode(n2))
                {
                    res = makeNode(getVarNode(n1),
                            app(cache, op, getFalseNode(n1), n2),
                            app(cache, op, getTrueNode(n1), n2));
                    cache[hash] = res;
                }
                else
                {
                    res = makeNode(getVarNode(n2),
                            app(cache, op, n1, getFalseNode(n2)),
                            app(cache, op, n1, getTrueNode(n2)));
                    cache[hash] = res;
                }
            }

            return res;
        }

        BddNode*
        restr(
            BddNode*  n,
            std::size_t     var,
            const BddNode*   value) noexcept
        {
            if (getVarNode(n) > var)
                 return n;
            else if (getVarNode(n) < var)
                return makeNode(getVarNode(n),
                        restr(getFalseNode(n), var, value),
                        restr(getTrueNode(n), var, value));
            else if (getVarNode(n) == var && isFalseBdd(value))
                return getFalseNode(n);
            else //getVarNde(n) == var && value == Bdd::getTrueNode(n)
                return getTrueNode(n);
        }

        void
        dfs(
            const std::function<void (BddNode*)>& op,
            BddNode* node) noexcept
        {
            op(node);

            if (isLeaf(node))
                return;

            dfs(op, getFalseNode(node));
            dfs(op, getTrueNode(node));
        }

        void
        bfs(
            const std::function<void (BddNode*)>& op,
            BddNode* node) noexcept
        {
            std::queue<BddNode*> queue;

            queue.push(node);
            queue.push(nullptr);

            while (!queue.empty())
            {
                BddNode* n = queue.front();
                queue.pop();

                if (n != nullptr)
                {
                    op(n);

                    if (!isLeaf(n))
                    {
                        queue.push(getFalseNode(n));
                        queue.push(getTrueNode(n));
                    }
                }
                else if(!queue.empty())
                    queue.push(nullptr);
            }
        }

        void
        dfs_modify(
            const std::function<BddNode* (BddNode*)>& op,
            BddNode* node) noexcept
        {
            node = op(node);

            if (isLeaf(node))
                return;

            dfs_modify(op, getFalseNode(node));
            dfs_modify(op, getTrueNode(node));
        }

        void
        bfs_modify(
            const std::function<BddNode* (BddNode*)>& op,
            BddNode* node) noexcept
        {
            std::queue<BddNode*> queue;

            queue.push(node);
            queue.push(nullptr);

            while (!queue.empty())
            {
                BddNode* n = queue.front();
                queue.pop();

                if (n != nullptr)
                {
                    n = op(n);

                    if (!isLeaf(n))
                    {
                        queue.push(getFalseNode(n));
                        queue.push(getTrueNode(n));
                    }
                }
                else if(!queue.empty())
                    queue.push(nullptr);
            }
        }
    }

    void
    BreadthFirstSearch(
        const std::function<void (const BddNode*)>& op,
        const Bdd& bdd) noexcept
    {
        bfs(op, bdd.getRoot());
    }

    void
    DepthFirstSearch(
        const std::function<void (const BddNode*)>& op,
        const Bdd& bdd) noexcept
    {
        dfs(op, bdd.getRoot());
    }

    void
    bfs_ModifyBdd(
        const std::function<BddNode* (BddNode*)>& op,
        Bdd& bdd) noexcept
    {
        bfs_modify(op, bdd.getRoot());
    }

    void
    dfs_ModifyBdd(
        const std::function<BddNode* (BddNode*)>& op,
        Bdd& bdd) noexcept
    {
        dfs_modify(op, bdd.getRoot());
    }

    Bdd
    apply(
        const std::function<BddNode* (const BddNode*, const BddNode*)>& op,
        const Bdd& bdd1,
        const Bdd& bdd2) noexcept
    {
        std::unordered_map<std::size_t, BddNode*> cache;
        return Bdd(app(cache, op, bdd1.getRoot(), bdd2.getRoot()));
    }

    Bdd
    Restrict(
        const Bdd&      n,
        std::size_t     var,
        BddNode*  value) noexcept
    {
        return Bdd(restr(n.getRoot(), var, value));
    }

    void
    setVarNum(std::size_t nbVar) noexcept
    {
        // Init the structure that stores the variables
        Caching::__variables.clear();
        Caching::__variables.resize(nbVar);

        // Init True and False nodes
        Caching::falseNode->var = nbVar + 1;
        Caching::trueNode->var = nbVar;

        for (std::size_t i = 0; i < nbVar; ++i)
            Caching::__variables[i] = i;
    }
}
