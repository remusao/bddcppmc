#ifndef CACHING_HH_
# define CACHING_HH_

# include <unordered_map>
# include <vector>
# include "memoryManager.hh"


namespace Bdd
{
    // Forward declaration
    struct BddNode;

    typedef std::unordered_map<std::size_t, BddNode*> UniqueHashMap;

    // Global caching structures
    // Hashtable :
    // 1) __id2node :: id -> (id', l, h)
    // on a l'id d'une node et on veut retrouver le pointeur -- USELESS si on
    // manipule du pointeur ?
    // 2) __node2id :: (id, l, h) -> id
    // on a un tuple(var, false, true) et on veut le pointeur vers la vrai node
    // static std::unordered_map<unsigned, std::unique_ptr<BddNode>>   __id2node; // 1)
    class Caching
    {
        public:

        static UniqueHashMap __node2id; // 2)

        // Vector used to store the global order on variables
        static std::vector<std::size_t> __variables;

        // Unique true and false nodes
        static struct BddNode* trueNode;
        static struct BddNode* falseNode;

        // Memory manager
        static MemoryManager<BddNode> g_mm;
    };

    // Do not use this method to test if a node is True
    inline BddNode*
    getTrueBdd() noexcept
    {
        return Caching::trueNode;
    }

    // Do not use this method to test if a node is False
    inline BddNode*
    getFalseBdd() noexcept
    {
        return Caching::falseNode;
    }
}

#endif
