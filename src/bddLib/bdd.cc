#include <climits>
#include <iostream>
#include <fstream>
#include <set>
#include <map>
#include <list>
#include <stack>

#include "bdd.hh"
#include "bddUtils.hh"
#include "caching.hh"

namespace Bdd
{
    Bdd::Bdd() noexcept
      : root_(getTrueBdd())
    {
    }

    Bdd::Bdd(BddNode* root) noexcept
        : root_(root)
    {
    }

    Bdd::~Bdd() noexcept
    {
    }

    // Init the memory manager
    void initBdd(std::size_t nbVars, std::size_t poolSize) noexcept
    {
        // init the memory manager
        Caching::g_mm = initMM<BddNode>(poolSize);
        Caching::__node2id.max_load_factor(0.8);

        // true and false nodes are the first nodes
        // allocated with the memory manager. Their
        // respective values are set in the setNbVar
        // function (later)
        Caching::trueNode = newNode(Caching::g_mm);
        Caching::falseNode = newNode(Caching::g_mm);

        setVarNum(nbVars);
    }

    void releaseBdd() noexcept
    {
        release(Caching::g_mm);
    }



    BddNode*
    Bdd::getRoot() const noexcept
    {
        return root_;
    }


    Bdd
    Bdd::operator!() const noexcept
    {
        static auto op = [this](const BddNode* n, const BddNode*) -> BddNode*
        {
            return isFalseBdd(n) ? getTrueBdd() : getFalseBdd();
        };
        return apply(op, *this, getTrueBdd());
    }

    Bdd
    Bdd::operator||(const Bdd& bdd) const noexcept
    {
        // http://gcc.gnu.org/bugzilla/show_bug.cgi?id=51494
        static auto op = [this](const BddNode* n1, const BddNode* n2) -> BddNode*
        {
            return ((isTrueBdd(n1) || isTrueBdd(n2)) ? getTrueBdd() : getFalseBdd());
        };
        return apply(op, *this, bdd);
    }

    Bdd
    Bdd::operator&&(const Bdd& bdd) const noexcept
    {
        // http://gcc.gnu.org/bugzilla/show_bug.cgi?id=51494
        static auto op = [this](const BddNode* n1, const BddNode* n2) -> BddNode*
        {
            return ((isTrueBdd(n1) && isTrueBdd(n2)) ? getTrueBdd() : getFalseBdd());
        };
        return apply(op, *this, bdd);
    }

    Bdd
    Bdd::operator^(const Bdd& bdd) const noexcept
    {
        // http://gcc.gnu.org/bugzilla/show_bug.cgi?id=51494
        static auto op = [this](const BddNode* n1, const BddNode* n2) -> BddNode*
        {
            return (isTrueBdd(n1) && isTrueBdd(n2)) || (isFalseBdd(n1) && isFalseBdd(n2)) ? getFalseBdd() : getTrueBdd();
        };
        return apply(op, *this, bdd);
    }

    Bdd
    Bdd::exist(std::size_t var) const noexcept
    {
        const Bdd& bdd1 = Restrict(*this, var, getFalseBdd());
        const Bdd& bdd2 = Restrict(*this, var, getTrueBdd());
        return bdd1 || bdd2;
    }

    Bdd
    Bdd::implyNot(const Bdd& bdd) const noexcept
    {
        static auto op = [this](const BddNode* n1, const BddNode* n2) -> BddNode*
        {
            return ((isFalseBdd(n1) || isFalseBdd(n2)) ? getTrueBdd() : getFalseBdd());
        };

        return apply(op, *this, bdd); // !*this || bdd;
    }

    Bdd
    Bdd::imply(const Bdd& bdd) const noexcept
    {
        static auto op = [this](const BddNode* n1, const BddNode* n2) -> BddNode*
        {
            return ((isFalseBdd(n1) || isTrueBdd(n2)) ? getTrueBdd() : getFalseBdd());
        };

        return apply(op, *this, bdd); // !*this || bdd;
    }

    Bdd
    Bdd::equivalence(const Bdd& bdd) const noexcept
    {
        return (*this && bdd) || (!*this && !bdd);
    }

    namespace
    {
        void print_op(const BddNode* n) noexcept
        {
            if (isLeaf(n))
                return;
            std::cout << "I" << getVarNode(n) << " = [" << getVarNode(n) << " ? ";
            if (isFalseBdd(getFalseNode(n)))
                std::cout << "false : ";
            else if (isTrueBdd(getFalseNode(n)))
                std::cout << "true : ";
            else
                std::cout << "I" << getVarNode(getFalseNode(n)) << " : ";

            if (isFalseBdd(getTrueNode(n)))
                std::cout << "false]" << std::endl;
            else if (isTrueBdd(getTrueNode(n)))
                std::cout << "true]" << std::endl;
            else
                std::cout << "I" << getVarNode(getTrueNode(n)) << "]" << std::endl;
        }
    }


    void
    Bdd::print() const noexcept
    {
        BreadthFirstSearch(print_op, *this);
    }

    std::vector<std::pair<unsigned, bool>>
    Bdd::any_sat () const noexcept
    {
        decltype(any_sat()) res;
        BddNode* cursor = this->root_;
        std::vector<BddNode*> node_stack;
        node_stack.push_back (cursor);

        while (!node_stack.empty())
        {
          cursor = node_stack.back();
          node_stack.pop_back();

          if (res.size() > 0)
            res.pop_back();

          BddNode* true_node = getTrueNode(cursor);
          BddNode* false_node = getFalseNode(cursor);

          if (isTrueBdd(true_node))
            break;
          else if (true_node && !isFalseBdd(true_node))
          {
            res.push_back(std::make_pair(getVarNode(true_node), true));
            node_stack.push_back(true_node);
          }

          if (isTrueBdd(false_node))
            break;
          else if (false_node && !isFalseBdd(false_node))
          {
            res.push_back(std::make_pair(getVarNode(false_node), false));
            node_stack.push_back(false_node);
          }
        }
        return res;

    }

    std::vector<std::vector<std::pair<unsigned, bool>>>
    Bdd::all_sat() const noexcept
    {
        decltype(all_sat()) res_total;
        BddNode* cursor = this->root_;
        std::vector<BddNode*> node_stack;
        node_stack.push_back (cursor);
        std::vector<std::pair<unsigned, bool>> res;

        while (!node_stack.empty())
        {
          cursor = node_stack.back();
          node_stack.pop_back();

          if (res.size() > 0)
            res.pop_back();

          BddNode* true_node = getTrueNode(cursor);
          BddNode* false_node = getFalseNode(cursor);

          if (isTrueBdd(true_node))
          {
            res_total.push_back(res);
            continue;
          }

          else if (true_node && !isFalseBdd(true_node))
          {
            std::pair<unsigned, bool> pair;
            res.push_back(
                std::pair<unsigned, bool>(getVarNode(true_node),
                                           true));
            node_stack.push_back(true_node);
          }

          if (isTrueBdd(false_node))
          {
            res_total.push_back(res);
            continue;
          }

          else if (false_node && !isFalseBdd(false_node))
          {
            res.push_back(
                std::pair<unsigned, bool>(getVarNode(false_node),
                false));
            node_stack.push_back(false_node);
          }
        }
        return res_total;
    }

    unsigned
    Bdd::sat() const noexcept
    {
        BddNode* cursor = this->root_;
        std::vector<BddNode*> node_stack;
        node_stack.push_back (cursor);
        unsigned total = 0;
        std::vector<std::pair<unsigned, bool>> res;

        while (!node_stack.empty())
        {
          cursor = node_stack.back();
          node_stack.pop_back();

          if (res.size() > 0)
            res.pop_back();

          BddNode* true_node = getTrueNode(cursor);
          BddNode* false_node = getFalseNode(cursor);

          if (isTrueBdd(true_node))
          {
            ++total;
            continue;
          }

          else if (true_node && !isFalseBdd(true_node))
          {
            std::pair<unsigned, bool> pair;
            res.push_back(
                std::pair<unsigned, bool>(getVarNode(true_node),
                                           true));
            node_stack.push_back(true_node);
          }

          if (isTrueBdd(false_node))
          {
            ++total;
            continue;
          }

          else if (false_node && !isFalseBdd(false_node))
          {
            res.push_back(
                std::pair<unsigned, bool>(getVarNode(false_node),
                false));
            node_stack.push_back(false_node);
          }
        }
        return total;
    }
}
