#include <iostream>
#include <string>
#include "bddLib/bdd.hh"

#define SIZE (5 * 4)

namespace
{
    Bdd::Bdd graph = Bdd::getTrueBdd();
    Bdd::Bdd states[SIZE];

    Bdd::Bdd edge(unsigned a, unsigned b)
    {
        Bdd::Bdd
            state1Color1, state1Color2, state1Color3, state1Color4,
            state2Color1, state2Color2, state2Color3, state2Color4;

        // One state, one color
        state1Color1 = states[a + 0];
        state1Color2 = states[a + 1];
        state1Color3 = states[a + 2];
        state1Color4 = states[a + 3];

        state2Color1 = states[b + 0];
        state2Color2 = states[b + 1];
        state2Color3 = states[b + 2];
        state2Color4 = states[b + 3];

        return
           state1Color1.imply(!state2Color1)
        && state1Color2.imply(!state2Color2)
        && state1Color3.imply(!state2Color3)
        && state1Color4.imply(!state2Color4)
        && (state1Color1 || state1Color2 || state1Color3 || state1Color4)
        && (state2Color1 || state2Color2 || state2Color3 || state2Color4);
    }


    Bdd::Bdd createGraph()
    {
        return edge(0, 4) && edge(0, 8) && edge(4, 16) && edge(8, 16);
    }
}

int
main(int, char *[])
{
    std::cout << "Graph coloration" << std::endl;

    // 48 states * 4 colors
    Bdd::initBdd(SIZE);

    for (unsigned i = 0; i < SIZE; ++i)
        states[i] = Bdd::bdd_ithvar(i);

    Bdd::Bdd usa = createGraph();
    std::cout << "There are " << usa.sat() << " solutions\n";
    std::cout << "one is:" << std::endl;
    auto any_sat = usa.any_sat();
    for (std::pair<int, bool> pair : any_sat)
        std::cout << "Var num : " << pair.first << " val : " << pair.second << std::endl;

    return 0;
}
