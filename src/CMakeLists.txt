# Add subdirectories

add_subdirectory(bddLib)

# Add the binary and sources
add_executable(
    bddMain
    main.cc
)

add_executable(
    bddQueen
    queen.cc
)

add_executable(
    coloration4
    coloration4.cc
)

add_executable(
    graphColoration
    graphColoration.cc
)

add_executable(
    bddKnights
    knights.cc
)

SET_TARGET_PROPERTIES(bdd PROPERTIES LINKER_LANGUAGE CXX)
target_link_libraries(bddMain bdd)
target_link_libraries(bddQueen bdd)
target_link_libraries(graphColoration bdd)
target_link_libraries(coloration4 bdd)
target_link_libraries(bddKnights bdd)
