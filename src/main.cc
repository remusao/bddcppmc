#include <iostream>
#include "bddLib/bdd.hh"

int
main(int, char *[])
{

    std::cout << "Setting var number" << std::endl;
    Bdd::initBdd(3);

	std::cout << "Hello BddWorld !" << std::endl;


    std::cout << "Creating Bdd a" << std::endl;
    Bdd::Bdd a = Bdd::bdd_ithvar(0);
    a.print();
    std::cout << "\nCreating Bdd b" << std::endl;
    Bdd::Bdd b = Bdd::bdd_ithvar(1);
    b.print();
    std::cout << "\nCreating Bdd c" << std::endl;
    Bdd::Bdd c = Bdd::bdd_ithvar(2);
    c.print();

    Bdd::Bdd abc = a && b && c;
    Bdd::Bdd bc = b && c;

    std::cout << "Printing abc" << std::endl;
    abc.print();
    std::cout << "Printing !bc" << std::endl;
    (!bc).print();
    std::cout << "Printing abc" << std::endl;
    abc.print();

    std::cout << "Printing !(a && b && c)" << std::endl;
    (!(a && b && c)).print();

    Bdd::Bdd res = (!(a && b && c));
    std::vector<std::pair<unsigned, bool>> res_arr = res.any_sat ();

    std::cout << "Size : " << res_arr.size() << std::endl;
    for (std::pair<unsigned, bool> p : res_arr)
    {
      std::cout << p.first << " " << p.second << std::endl;
    }

    Bdd::releaseBdd();

    return 0;
}

